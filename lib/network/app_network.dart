import 'package:dio/dio.dart';

class AppNetwork {
  final String _baseUrl = 'https://dummyjson.com';

  static final Dio _dio = Dio()
    ..options.connectTimeout = const Duration(seconds: 10);

  AppNetwork._();

  static final AppNetwork _instance = AppNetwork._();

  factory AppNetwork() {
    return _instance;
  }

  Future<Response> get({
    required String path,
    Map<String, dynamic>? data,
  }) async {
    try {
      final res = await _dio.get(
        _baseUrl + path,
        data: data,
      );
      return res;
    } catch (e) {
      rethrow;
    }
  }
}
