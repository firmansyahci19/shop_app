import 'package:flutter/material.dart';

class HomeMenuItem {
  final String title;
  final Widget icon;
  final Widget page;

  HomeMenuItem({
    required this.title,
    required this.icon,
    required this.page,
  });
}
