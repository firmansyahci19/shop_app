import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

import '../controllers/product_controller.dart';
import '../widgets/product_item.dart';

class ProductOverviewScreen extends StatelessWidget {
  ProductOverviewScreen({super.key});

  final ProductController productController = Get.put(ProductController());

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Obx(
        () => productController.isLoading.value
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : MasonryGridView.builder(
                shrinkWrap: true,
                gridDelegate:
                    const SliverSimpleGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2),
                mainAxisSpacing: 16,
                crossAxisSpacing: 16,
                itemCount: productController.items?.length,
                itemBuilder: (context, i) => GestureDetector(
                  onTap: () => productController
                      .goToProductDetailPage(productController.items![i]),
                  child: Obx(
                    () => ProductItem(
                      product: productController.items![i],
                      toogleFavoriteStatus: () => productController
                          .toggleFavoriteStatus(productController.items![i]),
                      isFavorite: productController
                          .isFavorite(productController.items![i]),
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
