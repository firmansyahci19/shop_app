import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/home_controller.dart';
import '../models/home_menu_item.dart';
import 'chat_screen.dart';
import 'product_favorite_screen.dart';
import 'product_overview_screen.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});

  final HomeController homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    final List<HomeMenuItem> homeWidgets = [
      HomeMenuItem(
        title: 'List',
        icon: const Icon(Icons.list),
        page: ProductOverviewScreen(),
      ),
      HomeMenuItem(
        title: 'Favorite',
        icon: const Icon(Icons.favorite),
        page: ProductFavoriteScreen(),
      ),
      HomeMenuItem(
        title: 'Chat',
        icon: const Icon(Icons.message),
        page: const ChatScreen(),
      ),
    ];
    return Obx(
      () => Scaffold(
        body: SafeArea(
          child: PageView(
            controller: homeController.pageController,
            onPageChanged: homeController.onSelectedMenu,
            children: [
              ProductOverviewScreen(),
              ProductFavoriteScreen(),
              const ChatScreen(),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: homeController.navIndex.value,
          onTap: homeController.onSelectedMenu,
          items: homeWidgets
              .map(
                (menu) => BottomNavigationBarItem(
                  label: menu.title,
                  icon: menu.icon,
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
