import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_app/models/products_response.dart';

class ProductDetailScreen extends StatelessWidget {
  ProductDetailScreen({super.key});

  final Product? product = Get.arguments as Product?;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              Stack(
                children: [
                  Image.network(product?.thumbnail ?? ''),
                ],
              ),
              Text(product?.title ?? ''),
              Text(product?.price.toString() ?? ''),
              Text(product?.description ?? ''),
              const SizedBox(height: 16),
              Expanded(
                child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    mainAxisSpacing: 8,
                    crossAxisSpacing: 8,
                  ),
                  itemBuilder: (context, i) =>
                      Image.network(product?.images?[i] ?? ''),
                  itemCount: product?.images?.length,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
