import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/product_controller.dart';

class ProductFavoriteScreen extends StatelessWidget {
  ProductFavoriteScreen({super.key});

  final ProductController productController = Get.find<ProductController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: ListView.builder(
        itemCount: productController.favItems?.length,
        itemBuilder: (context, i) => GestureDetector(
          onTap: () => productController
              .goToProductDetailPage(productController.favItems![i]),
          child: Padding(
            padding: const EdgeInsets.all(2.0),
            child: Row(
              children: [
                SizedBox(
                  height: 80,
                  width: 80,
                  child: Image.network(
                    productController.favItems?[i].thumbnail ?? '',
                    fit: BoxFit.cover,
                  ),
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(productController.favItems?[i].title ?? ''),
                      Text(productController.favItems?[i].price.toString() ??
                          ''),
                      Text(productController.favItems?[i].description ?? ''),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
