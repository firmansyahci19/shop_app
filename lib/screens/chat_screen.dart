import 'package:flutter/material.dart';

import '../widgets/message_item.dart';

class ChatScreen extends StatelessWidget {
  const ChatScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: const EdgeInsets.all(16),
          child: const Row(
            children: [
              Icon(Icons.arrow_back_ios),
              Expanded(
                child: Row(
                  children: [
                    CircleAvatar(),
                    SizedBox(width: 8),
                    Text('Diana Summers'),
                    SizedBox(width: 8),
                    Icon(Icons.check),
                  ],
                ),
              ),
              Icon(Icons.video_call_outlined),
              SizedBox(width: 8),
              Icon(Icons.call_outlined),
            ],
          ),
        ),
        const Divider(),
        Expanded(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const MessageItem(
                  isFromMe: true,
                  message: 'Hey could you join me today?',
                ),
                GestureDetector(
                  onDoubleTap: () {},
                  child: const MessageItem(
                    message:
                        'That`s sounds great! I`m in. What time works for you?',
                  ),
                ),
                const MessageItem(
                  isFromMe: true,
                  message:
                      'I`ll be ready around 12 o`clock. do you want to meet you there',
                ),
              ],
            ),
          ),
        ),
        const Divider(),
        Container(
          padding: const EdgeInsets.all(8),
          child: const Row(
            children: [
              Icon(Icons.camera_alt_outlined),
              SizedBox(width: 12),
              Expanded(
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Message...',
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1),
                    ),
                    suffixText: 'Send',
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
