import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MessageItem extends StatelessWidget {
  final bool isFromMe;
  final String? message;

  const MessageItem({this.isFromMe = false, this.message, super.key});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isFromMe ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        margin: const EdgeInsets.only(bottom: 12),
        width: (Get.width - 60),
        padding: const EdgeInsets.all(2),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: isFromMe ? Colors.orangeAccent : Colors.grey),
        child: Text(
          message ?? '',
          style: TextStyle(
            color: isFromMe ? Colors.white : Colors.black,
          ),
        ),
      ),
    );
  }
}
