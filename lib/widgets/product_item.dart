import 'package:flutter/material.dart';

import '../models/products_response.dart';

class ProductItem extends StatelessWidget {
  final Product? product;
  final VoidCallback? toogleFavoriteStatus;
  final bool? isFavorite;

  const ProductItem(
      {this.product, this.toogleFavoriteStatus, this.isFavorite, super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2),
      child: Column(
        children: [
          Stack(
            children: [
              Image.network(product?.thumbnail ?? ''),
              Positioned(
                child: IconButton(
                  icon: (isFavorite ?? false)
                      ? const Icon(
                          Icons.favorite,
                          color: Colors.red,
                        )
                      : const Icon(Icons.favorite_outline),
                  onPressed: toogleFavoriteStatus,
                ),
              ),
            ],
          ),
          Text(product?.title ?? ''),
        ],
      ),
    );
  }
}
