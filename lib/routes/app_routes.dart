class AppRoutes {
  static const String home = '/';
  static const String productOverview = '/product-overview';
  static const String productDetail = '/product-detail';
  static const String productFavorite = '/product-favorite';
  static const String chat = '/chat';
}
