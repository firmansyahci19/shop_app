import 'package:get/get.dart';

import '../screens/chat_screen.dart';
import '../screens/home_screen.dart';
import '../screens/product_detail_screen.dart';
import '../screens/product_favorite_screen.dart';
import '../screens/product_overview_screen.dart';
import 'app_routes.dart';

class AppPages {
  static List<GetPage> generateRoute() {
    return [
      GetPage(
        name: AppRoutes.home,
        page: () => HomeScreen(),
      ),
      GetPage(
        name: AppRoutes.productOverview,
        page: () => ProductOverviewScreen(),
      ),
      GetPage(
        name: AppRoutes.productDetail,
        page: () => ProductDetailScreen(),
      ),
      GetPage(
        name: AppRoutes.productFavorite,
        page: () => ProductFavoriteScreen(),
      ),
      GetPage(
        name: AppRoutes.chat,
        page: () => const ChatScreen(),
      ),
    ];
  }
}
