import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  final pageController = PageController(
    initialPage: 0,
  );

  Rx<int> navIndex = 0.obs;

  onSelectedMenu(int value) {
    pageController.jumpToPage(value);
    navIndex.value = value;
    update();
  }

  @override
  void dispose() {
    pageController.dispose();

    super.dispose();
  }
}
