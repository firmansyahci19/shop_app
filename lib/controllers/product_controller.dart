import 'dart:developer';

import 'package:get/get.dart';

import '../const/api_path.dart';
import '../models/products_response.dart';
import '../network/app_network.dart';
import '../routes/app_routes.dart';

class ProductController extends GetxController {
  final AppNetwork _appNetwork = AppNetwork();

  List<Product>? items = [];

  RxList<Product>? favItems = <Product>[].obs;

  Rx<bool> isLoading = false.obs;

  @override
  void onInit() {
    super.onInit();

    getProducts();
  }

  Future<void> getProducts() async {
    try {
      _setIsLoading(true);
      final res = await _appNetwork.get(path: ApiPath.products);
      if (res.statusCode == 200) {
        final productRes = ProductsResponse.fromJson(res.data);
        items = productRes.products;
      }
      _setIsLoading(false);
    } catch (e) {
      _setIsLoading(false);
      log(e.toString());
    }
  }

  goToProductDetailPage(Product? product) {
    Get.toNamed(AppRoutes.productDetail, arguments: product);
  }

  _setIsLoading(bool value) {
    isLoading.value = value;
    update();
  }

  toggleFavoriteStatus(Product product) {
    final bool? isExist = favItems?.contains(product);
    if (isExist ?? false) {
      favItems?.remove(product);
    } else {
      favItems?.add(product);
    }
    update();
  }

  bool? isFavorite(Product product) {
    final bool? isExist = favItems?.contains(product);
    return isExist ?? false;
  }
}
